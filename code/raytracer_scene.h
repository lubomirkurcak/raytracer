/* ========================================================================
   $File: $
   $Date: $
   $Revision: $
   $Creator: Lubomir Kurcak $
   $Notice: (C) Copyright 2018 All Rights Reserved. $
   ======================================================================== */

struct hitable;

struct scene
{
    s32 ObjectCount;
    hitable *Objects;
    hitable **Refs;
    hitable *Root;
};

