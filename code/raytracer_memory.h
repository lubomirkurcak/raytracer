/* ========================================================================
   $File: $
   $Date: $
   $Revision: $
   $Creator: Lubomir Kurcak $
   $Notice: (C) Copyright 2018 All Rights Reserved. $
   ======================================================================== */

#include <malloc.h>

#define AllocateArray(type, Count) (type*)MemoryAlloc(sizeof(type)*(Count))

inline void *
MemoryAlloc(size_t Size)
{
    void *Result = malloc(Size);
    return Result;
}

inline void
Free(void *Memory)
{
    free(Memory);
}

