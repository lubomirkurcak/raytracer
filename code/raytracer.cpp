/* ========================================================================
   $File: $
   $Date: $
   $Revision: $
   $Creator: Lubomir Kurcak $
   $Notice: (C) Copyright 2018 All Rights Reserved. $
   ======================================================================== */

#define MAX_BOUNCES 8
#define OUTPUT_WIDTH 1920/2
#define OUTPUT_HEIGHT 1080/2

#include "raytracer.h"
#include "raytracer_scene.cpp"

inline ray
GetRay(camera *Camera, f32 S, f32 T, random_series *Series)
{
    v3 Random = Camera->LensRadius*GetRandomV3FromUnitDisk(Series);
    v3 Offset = Random.x*Camera->U + Random.y*Camera->V;
    v3 RayOrigin = Camera->Origin + Offset;
    
    ray Ray;
    Ray.A = RayOrigin;
    Ray.Dir = Camera->LowerLeftCorner + S*Camera->Horizontal + T*Camera->Vertical - RayOrigin;
    
    return Ray;
}

inline recti
GetWholeImage(image *Image)
{
    recti Result = RectiMinDim({0,0}, {Image->Width, Image->Height});
    return Result;
}

inline aabb
BoundingBoxUnion(aabb A, aabb B)
{
    aabb Result;
    Result.Min.x = Minimum(A.Min.x, B.Min.x);
    Result.Min.y = Minimum(A.Min.y, B.Min.y);
    Result.Min.z = Minimum(A.Min.z, B.Min.z);
    Result.Max.x = Maximum(A.Max.x, B.Max.x);
    Result.Max.y = Maximum(A.Max.y, B.Max.y);
    Result.Max.z = Maximum(A.Max.z, B.Max.z);

    return Result;
}

BOUNDING_BOX_FUNCTION(SphereBoundingBox, sphere *Sphere)
{
    b32 Success = true;
    v3 Radius = V3(Sphere->Radius, Sphere->Radius, Sphere->Radius);
    Result->Min = Sphere->Center - Radius;
    Result->Max = Sphere->Center + Radius;

    return Success;
}

BOUNDING_BOX_FUNCTION(HitableBoundingBox, hitable *Hitable)
{
    b32 Success = false;
    
    switch(Hitable->Type)
    {
        case Hit_BVHNode:
        {
            *Result = Hitable->BVHNode.AABB;
            Success = true;
        } break;
        
        case Hit_Sphere:
        {
            Success = SphereBoundingBox(tMin, tMax, Result, &Hitable->Sphere);
        } break;

        InvalidDefaultCase;
    }

    return Success;
}

#define COMPARE_FUNCTION(name) int name(const void *A, const void *B)
typedef COMPARE_FUNCTION(compare_function);

COMPARE_FUNCTION(BoxXCompare)
{
    s32 Result = 0;
    aabb LeftBox;
    aabb RightBox;
    hitable *Left = *(hitable **)A;
    hitable *Right = *(hitable **)B;    
    b32 LeftSuccess = HitableBoundingBox(0, 0, &LeftBox, Left);
    b32 RightSuccess = HitableBoundingBox(0, 0, &RightBox, Right);
    Assert(LeftSuccess && RightSuccess);
    Result = (LeftBox.Min.E[0] < RightBox.Min.E[0]) ? -1 : 1;
    return Result;
}

COMPARE_FUNCTION(BoxYCompare)
{
    s32 Result = 0;
    aabb LeftBox;
    aabb RightBox;
    hitable *Left = *(hitable **)A;
    hitable *Right = *(hitable **)B;    
    b32 LeftSuccess = HitableBoundingBox(0, 0, &LeftBox, Left);
    b32 RightSuccess = HitableBoundingBox(0, 0, &RightBox, Right);
    Assert(LeftSuccess && RightSuccess);
    Result = (LeftBox.Min.E[1] < RightBox.Min.E[1]) ? -1 : 1;
    return Result;
}

COMPARE_FUNCTION(BoxZCompare)
{
    s32 Result = 0;
    aabb LeftBox;
    aabb RightBox;
    hitable *Left = *(hitable **)A;
    hitable *Right = *(hitable **)B;    
    b32 LeftSuccess = HitableBoundingBox(0, 0, &LeftBox, Left);
    b32 RightSuccess = HitableBoundingBox(0, 0, &RightBox, Right);
    Assert(LeftSuccess && RightSuccess);
    Result = (LeftBox.Min.E[2] < RightBox.Min.E[2]) ? -1 : 1;
    return Result;
}

internal void
Quicksort(void *Array, size_t Count, size_t SizeOfOneElement, compare_function *Compare)
{
    qsort(Array, Count, SizeOfOneElement, Compare);
}

internal hitable *
CreateBVH(scene *Scene, hitable **Hitables, s32 Count, f32 t0, f32 t1, random_series *Series)
{
    if(Count == 0)
    {
        return 0;
    }
    
    hitable *Object = NewSceneObject(Scene, Hit_BVHNode);
    bvh_node *Node = &Object->BVHNode;
    
    s32 Axis = (s32)(3*GetRandomF32(Series));
    if(Axis == 0)
    {
        Quicksort(Hitables, Count, sizeof(hitable *), BoxXCompare);
    }
    else if(Axis == 1)
    {
        Quicksort(Hitables, Count, sizeof(hitable *), BoxYCompare);
    }
    else
    {
        Quicksort(Hitables, Count, sizeof(hitable *), BoxZCompare);
    }

    if(Count == 1)
    {
        Node->Left = Node->Right = Hitables[0];
    }
    else if(Count == 2)
    {
        Node->Left = Hitables[0];
        Node->Right = Hitables[1];
    }
    else
    {
        s32 LeftCount = Count / 2;
        s32 RightCount = Count - LeftCount;
        Node->Left = CreateBVH(Scene, Hitables, LeftCount, t0, t1, Series);
        Node->Right = CreateBVH(Scene, Hitables + LeftCount, RightCount, t0, t1, Series);
    }

    aabb LeftBox;
    aabb RightBox;
    b32 LeftSuccess = HitableBoundingBox(t0, t1, &LeftBox, Node->Left);
    b32 RightSuccess = HitableBoundingBox(t0, t1, &RightBox, Node->Right);
    Node->AABB = BoundingBoxUnion(LeftBox, RightBox);

    return Object;
}

HIT_FUNCTION(HitHitable, hitable *Hitable);

HIT_FUNCTION(HitSphere, v3 Center, f32 Radius)
{
    b32 Result = false;
    v3 OriginCenter = Ray->A - Center;
    v3 Direction = Ray->Dir;

    float A = Inner(Direction, Direction);
    float B = Inner(OriginCenter, Direction);
    float C = Inner(OriginCenter, OriginCenter) - Square(Radius);
    float D = B*B - A*C;
    
    if(D > 0)
    {
        f32 SqrtD = SquareRoot(D);
        f32 t = (-B - SqrtD)/A;
        Result = t > tMin && t < tMax;
        if(!Result)
        {
            t = (-B + SqrtD)/A;
            Result = t > tMin && t < tMax;
        }
        
        if(Result)
        {
            Record->t = t;
            Record->P = PointAtParameter(Ray, t);
            Record->Normal = (Record->P - Center) * (1/Radius);
        }
    }
    
    return Result;
}

HIT_FUNCTION(HitBVHNode, bvh_node *BVHNode)
{
    b32 Result = false;
    if(HitAABB(Ray, &BVHNode->AABB, tMin, tMax))
    {
        hit_record LeftHit;
        hit_record RightHit;
        b32 HitLeft = HitHitable(Ray, tMin, tMax, &LeftHit, BVHNode->Left);
        b32 HitRight = HitHitable(Ray, tMin, tMax, &RightHit, BVHNode->Right);

        if(HitLeft && HitRight)
        {
            if(LeftHit.t < RightHit.t)
            {
                *Record = LeftHit;
            }
            else
            {
                *Record = RightHit;
            }

            Result = true;
        }
        else if(HitLeft)
        {
            *Record = LeftHit;
            Result = true;
        }
        else if(HitRight)
        {
            *Record = RightHit;
            Result = true;
        }
    }
    
    return Result;
}

HIT_FUNCTION(HitHitable, hitable *Hitable)
{
    b32 Result = false;

    switch(Hitable->Type)
    {
        case Hit_BVHNode:
        {
            Result = HitBVHNode(Ray, tMin, tMax, Record, &Hitable->BVHNode);
        } break;
        
        case Hit_Sphere:
        {
            Result = HitSphere(Ray, tMin, tMax, Record,
                               Hitable->Sphere.Center, Hitable->Sphere.Radius);
            
            Record->Material = &Hitable->Material;
        } break;

        InvalidDefaultCase;
    }

    return Result;
}

#if 0
HIT_FUNCTION(HitMany, hitable *Hitables, s32 HitableCount)
{
    b32 Result = false;
    f32 ClosestSoFar = tMax;
    
    hitable *Hitable = Hitables;
    for(s32 Index=0; Index<HitableCount; ++Index)
    {
        hit_record CurrentHit;
        if(HitHitable(Ray, tMin, ClosestSoFar, &CurrentHit, Hitable))
        {
            Result = true;
            ClosestSoFar = CurrentHit.t;
            *Record = CurrentHit;
        }
        
        ++Hitable;
    }

    return Result;
}
#endif

MATERIAL_FUNCTION(DielectricScatter)
{
    b32 Result = true;

    *Attenuation = V3(1, 1, 1);

    v3 OutwardNormal;
    f32 RefractiveIndexRatio;
    f32 Cosine;
    if(Inner(Ray->Dir, Record->Normal) > 0)
    {
        OutwardNormal = -Record->Normal;
        RefractiveIndexRatio = Material->RefractiveIndex;
        Cosine = Material->RefractiveIndex * Inner(UnitVector(Ray->Dir), Record->Normal);
    }
    else
    {
        OutwardNormal = Record->Normal;
        RefractiveIndexRatio = 1 / Material->RefractiveIndex;
        Cosine = -Inner(UnitVector(Ray->Dir), Record->Normal);
    }

    v3 Refracted;
    f32 ReflectProbability = 1.0f;
    if(Refract(Ray->Dir, OutwardNormal, RefractiveIndexRatio, &Refracted))
    {
        ReflectProbability = Schlick(Cosine, Material->RefractiveIndex);
    }
    
    if(ReflectProbability < GetRandomF32(Entropy))
    {
        Scattered->A = Record->P;
        Scattered->Dir = Refracted;
    }
    else
    {
        v3 Reflected = Reflect(Ray->Dir, Record->Normal);
        Scattered->A = Record->P;
        Scattered->Dir = Reflected;
    }
    
    return Result;
}

MATERIAL_FUNCTION(LambertianScatter)
{
    b32 Result = true;
    
    v3 Target = Record->P + Record->Normal + GetRandomV3FromUnitSphere(Entropy);
    Scattered->A = Record->P;
    Scattered->Dir = Target - Record->P;
    *Attenuation = Material->Albedo;

    return Result;
}

MATERIAL_FUNCTION(MetalScatter)
{
    b32 Result;
    
    v3 Reflected =
        Reflect(UnitVector(Ray->Dir), Record->Normal) +
        Material->Fuzziness*GetRandomV3FromUnitSphere(Entropy);
    Scattered->A = Record->P;
    Scattered->Dir = Reflected;
    *Attenuation = Material->Albedo;

    Result = Inner(Scattered->Dir, Record->Normal) > 0;
    return Result;
}

internal b32
MaterialScatter(material *Material, ray *Ray, hit_record *Record, v3 *Attenuation, ray *Scattered, random_series *Entropy)
{
    b32 Result = false;

    switch(Material->Type)
    {
        case Mat_Diffuse:
        {
            Result = LambertianScatter(Material, Ray, Record, Attenuation, Scattered, Entropy);
        } break;
        
        case Mat_Metallic:
        {
            Result = MetalScatter(Material, Ray, Record, Attenuation, Scattered, Entropy);
        } break;

        case Mat_Dielectric:
        {
            Result = DielectricScatter(Material, Ray, Record, Attenuation, Scattered, Entropy);
        } break;
        
        InvalidDefaultCase;
    }

    return Result;
}

// TODO(lubomir): Make this non-recursive
// TODO(lubomir): Make depth parametrized from work_queue
inline v3
ComputeColor(scene *Scene, ray *Ray, s32 Depth, random_series *Entropy)
{
    v3 Result;
    hit_record Record;

//    if(HitMany(Ray, 0.001, F32_MAX, &Record, WorldObjects, WorldObjectCount))

    if(HitHitable(Ray, 0.001, F32_MAX, &Record, Scene->Root))
    {
        ray Scattered;
        v3 Attenuation;

        if(Depth < MAX_BOUNCES &&
           MaterialScatter(Record.Material, Ray, &Record, &Attenuation, &Scattered, Entropy))
        {
            v3 ScatteredColor = ComputeColor(Scene, &Scattered, Depth + 1, Entropy);
            Result = Hadamard(Attenuation, ScatteredColor);
        }
        else
        {
            Result = {0,0,0};
        }
    }
    else
    {
        v3 UnitDirection = UnitVector(Ray->Dir);
        f32 t = 0.5f*(UnitDirection.y + 1);

        v3 UpColor = {0.5,0.7,1};
        v3 DownColor = {1,1,1};
        Result = Lerp(DownColor, UpColor, t);
    }
    
    return Result;
}

// SCENES

// TODO(lubomir): Remove hacks
internal scene
GenerateScene1()
{
    scene Scene = {};

    Scene.Objects = GlobalSceneObjects;
    Scene.Refs = GlobalSceneObjectRefs;

    {
        sphere Sphere;
        Sphere.Center = {0,0,-1};
        Sphere.Radius = 0.5;

        material Material;
        Material.Type = Mat_Diffuse;
        Material.Albedo = V3(0.8, 0.3, 0.3);

        hitable Object;
        Object.Type = Hit_Sphere;
        Object.Sphere = Sphere;
        Object.Material = Material;

        AddObjectToScene(&Scene, Object);
    }
    {
        sphere Sphere;
        Sphere.Center = {0,-100.5f,-1};
        Sphere.Radius = 100;

        material Material;
        Material.Type = Mat_Diffuse;
        Material.Albedo = V3(0.5, 0.5, 0.5);

        hitable Object;
        Object.Type = Hit_Sphere;
        Object.Sphere = Sphere;
        Object.Material = Material;

        AddObjectToScene(&Scene, Object);
    }
    {
        sphere Sphere;
        Sphere.Center = {1,0,-1};
        Sphere.Radius = 0.5;

        material Material;
        Material.Type = Mat_Metallic;
        Material.Albedo = V3(0.8, 0.6, 0.2);
        Material.Fuzziness = 0.3;

        hitable Object;
        Object.Type = Hit_Sphere;
        Object.Sphere = Sphere;
        Object.Material = Material;

        AddObjectToScene(&Scene, Object);
    }
    {
        sphere Sphere;
        Sphere.Center = {-1,0,-1};
        Sphere.Radius = 0.5;

        material Material;
        Material.Type = Mat_Dielectric;
        Material.RefractiveIndex = 1.5;

        hitable Object;
        Object.Type = Hit_Sphere;
        Object.Sphere = Sphere;
        Object.Material = Material;

        AddObjectToScene(&Scene, Object);
    }

    return Scene;
}

s32 DrawToImageBufferRegion(work_queue *Queue)
{
    u32 WorkOrderIndex = LockedAddAndReturnOriginalValue(&Queue->NextWorkOrder, 1);
    if(WorkOrderIndex >= Queue->WorkOrderCount)
    {
        return 0;
    }
    
    work_order *Order = Queue->WorkOrders + WorkOrderIndex;

    random_series Entropy = Order->Entropy;
    image *Image = Order->Image;
    recti Region = Order->Region;
    camera *Camera = Order->Camera;
    scene *Scene = Order->Scene;
    
    s32 RaysPerPixel = Queue->RaysPerPixel;
    f32 InvWidth = 1 / (f32)Image->Width;
    f32 InvHeight = 1 / (f32)Image->Height;

    for(int Y=Region.Min.Y; Y<Region.Max.Y; ++Y)
    {
        u32 *Pixel = Image->Pixels + Y*Image->Width + Region.Min.X;
        
        for(int X=Region.Min.X; X<Region.Max.X; ++X)
        {
            v3 Color = {};
            
            for(s32 RayIndex=0; RayIndex<RaysPerPixel; ++RayIndex)
            {
                f32 U = ((f32)X + GetRandomF32(&Entropy)) * InvWidth;
                f32 V = ((f32)Y + GetRandomF32(&Entropy)) * InvHeight;

                ray Ray = GetRay(Camera, U, V, &Entropy);
                Color = Color + ComputeColor(Scene, &Ray, 0, &Entropy);
            }

            Color = Color * (1/(f32)RaysPerPixel);

            Color.r = LinearToGamma(Color.r);
            Color.g = LinearToGamma(Color.g);
            Color.b = LinearToGamma(Color.b);
            
            s32 R = RoundF32ToS32(255.0f * Color.r);
            s32 G = RoundF32ToS32(255.0f * Color.g);
            s32 B = RoundF32ToS32(255.0f * Color.b);

            u32 PixelValue = (B << 16) | (G << 8) | (R << 0);
            *Pixel++ = PixelValue;
        }
    }

    LockedAddAndReturnOriginalValue(&Queue->FinishedTilesCount, 1);
    
    return 1;
}

void DrawToImageBuffer(image *Image, camera *Camera, scene *Scene)
{
    recti WholeScreen = GetWholeImage(Image);
    
    int CoreCount = GetCPUCoreCount() * 2;
    int RegionSize = 64;

    u32 TilesWidth = DivideRoundUp(Image->Width, RegionSize);
    u32 TilesHeight = DivideRoundUp(Image->Height, RegionSize);
    u32 TotalTiles = TilesWidth*TilesHeight;

    work_queue Queue = {};
    Queue.WorkOrders = (work_order *)malloc(TotalTiles * sizeof(work_order));
    Queue.MaxBounceCount = 8;
    Queue.RaysPerPixel = 1;
    
    for(int Y=0; Y<Image->Height; Y += RegionSize)
    {
        for(int X=0; X<Image->Width; X += RegionSize)
        {
            recti Region = {X,Y,X+RegionSize,Y+RegionSize};
            Region = Intersect(Region, WholeScreen);

            work_order *Order = Queue.WorkOrders + Queue.WorkOrderCount++;
            Assert(Queue.WorkOrderCount <= TotalTiles);

            Order->Scene = Scene;
            Order->Image = Image;
            Order->Region = Region;
            Order->Camera = Camera;

            Order->Entropy = {(u32)(Y*Image->Width + X)};
        }
    }
    Assert(Queue.WorkOrderCount == TotalTiles);

    // Memory Barrier
    LockedAddAndReturnOriginalValue(&Queue.WorkOrderCount, 0);
    
    clock_t StartClock = clock();

    for(s32 CoreIndex=1; CoreIndex<CoreCount; ++CoreIndex)
    {
        CreateWorkThread(&Queue);
    }
    
    while(Queue.FinishedTilesCount < TotalTiles)
    {
        if(DrawToImageBufferRegion(&Queue))
        {
            fprintf(stderr, "\rRendering %d%%...", 100*(u32)Queue.FinishedTilesCount / TotalTiles);
            fflush(stdout);
        }
    }
    
    clock_t EndClock = clock();
    clock_t TimeElapsed = EndClock - StartClock;

    fprintf(stderr, "\rTotal time: %dms\n", TimeElapsed);
    fflush(stdout);
}

void OutputPPM(image *Image)
{
    printf("P3\n%d %d\n255\n", Image->Width, Image->Height);

    for(int Y=Image->Height-1; Y>=0; --Y)
    {
        u32 *Pixel = Image->Pixels + Y*Image->Width;
        for(int X=0; X<Image->Width; ++X)
        {
            u32 PixelValue = *Pixel++;

            u32 R = (PixelValue >>  0) & 0xFF;
            u32 G = (PixelValue >>  8) & 0xFF;
            u32 B = (PixelValue >> 16) & 0xFF;
            
            printf("%u %u %u\n", R, G, B);
        }
    }
}

internal void
DrawEntropyToImageBuffer(image *Image, random_series *Entropy)
{
    recti Region = GetWholeImage(Image);
    Region = CenteredIn(Region, {256,256});
    
    for(int Y=Region.Min.Y; Y<Region.Max.Y; ++Y)
    {
        u32 *Pixel = Image->Pixels + Y*Image->Width + Region.Min.X;
        
        for(int X=Region.Min.X; X<Region.Max.X; ++X)
        {
            v3 Color = {};

#if 1
            Color.r = Color.g = Color.b = GetRandomF32(Entropy);
#else
            Color.r = GetRandomF32(Entropy);
            Color.g = GetRandomF32(Entropy);
            Color.b = GetRandomF32(Entropy);
#endif

#if 0
            Color.r = LinearToGamma(Color.r);
            Color.g = LinearToGamma(Color.g);
            Color.b = LinearToGamma(Color.b);
#endif
            
            s32 R = RoundF32ToS32(255.0f * Color.r);
            s32 G = RoundF32ToS32(255.0f * Color.g);
            s32 B = RoundF32ToS32(255.0f * Color.b);

            u32 PixelValue = (B << 16) | (G << 8) | (R << 0);
            *Pixel++ = PixelValue;
        }
    }
}

int main()
{
    image Image;
    Image.Width = OUTPUT_WIDTH;
    Image.Height = OUTPUT_HEIGHT;
    Image.Pixels = AllocateArray(u32, Image.Width*Image.Height);
    
    GlobalSceneObjects = AllocateArray(hitable, MAX_SCENE_OBJECTS);
    GlobalSceneObjectRefs = AllocateArray(hitable *, MAX_SCENE_OBJECTS);

    v3 LookFrom = V3(3,3,2);
    v3 LookAt = V3(0,0,-1);
    v3 Up = V3(0,1,0);
    f32 FOV = DegreesToRadians(20);
    f32 AspectRatio = SafeRatio1((f32)OUTPUT_WIDTH, (f32)OUTPUT_HEIGHT);
    f32 FocusDistance = Length(LookFrom - LookAt);
    f32 Aperture = 0.5;
    
    camera Camera = CreateCamera(LookFrom, LookAt, Up, FOV, AspectRatio, Aperture, FocusDistance);

    
    scene Scene1 = GenerateScene1();
    scene *Scene = &Scene1;

#if 1
    random_series BVHEntropy = {456443131};
    Scene->Root = CreateBVH(Scene, Scene->Refs, Scene->ObjectCount, 0, 1, &BVHEntropy);
#endif
    
    DrawToImageBuffer(&Image, &Camera, Scene);

#if 0
    random_series EntropyTest = {12345};
    DrawEntropyToImageBuffer(&Image, &EntropyTest);
#endif
    
    OutputPPM(&Image);

    return 0;
}

#include "win32_raytracer.cpp"
