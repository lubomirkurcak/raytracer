/* ========================================================================
   $File: $
   $Date: $
   $Revision: $
   $Creator: Lubomir Kurcak $
   $Notice: (C) Copyright 2018 All Rights Reserved. $
   ======================================================================== */

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <time.h>
//#include <pthread.h>

typedef int32_t s32;
typedef uint32_t u32;
typedef s32 b32;

typedef float f32;
typedef double f64;

#define U32Min 0x0
#define U32Max 0xFFFFFFFF

#define internal static
#define global_variable static

#define F32_MAX 3.402823e+38
#define F32_MIN_POSITIVE 1.175494e-38

#define ArrayCount(Array) (sizeof(Array)/sizeof(Array[0]))

#define Minimum(A,B) (((A)<(B))?(A):(B))
#define Maximum(A,B) (((A)>(B))?(A):(B))

#define Assert(Expr) if(!(Expr)) InvalidCodePath
#define InvalidCodePath *(int *)0=0
#define InvalidDefaultCase default: InvalidCodePath

#include "raytracer_math.h"
#include "raytracer_random.h"
#include "raytracer_memory.h"
#include "raytracer_scene.h"

struct image
{
    s32 Width;
    s32 Height;
    u32 *Pixels;
};

struct ray
{
    v3 A;
    v3 Dir;
};

struct sphere
{
    v3 Center;
    f32 Radius;
};

struct aabb
{
    v3 Min;
    v3 Max;
};

enum material_type
{
    Mat_None,
    
    Mat_Diffuse,
    Mat_Metallic,
    Mat_Dielectric,
};

struct material
{
    material_type Type;

    v3 Albedo;
    f32 Fuzziness;
    f32 RefractiveIndex;
};

struct hit_record
{
    float t;
    v3 P;
    v3 Normal;

    material *Material;
};

enum hitable_type
{
    Hit_None,

    Hit_BVHNode,
    
    Hit_Sphere,
};

struct hitable;

struct bvh_node
{
    aabb AABB;
    hitable *Left;
    hitable *Right;
};

struct hitable
{
    hitable_type Type;

    material Material;
    
    union
    {
        sphere Sphere;
        bvh_node BVHNode;
    };
};

struct camera
{
    v3 Origin;
    v3 LowerLeftCorner;
    v3 Horizontal;
    v3 Vertical;

    v3 U;
    v3 V;
    v3 W;

    f32 LensRadius;
};

struct work_order
{
    scene *Scene;
    image *Image;
    recti Region;
    camera *Camera;
    random_series Entropy;
};

struct work_queue
{
    u32 MaxBounceCount;
    u32 RaysPerPixel;

    u32 WorkOrderCount;
    work_order *WorkOrders;
    
    volatile u32 NextWorkOrder;
    volatile u32 FinishedTilesCount;
};

inline v3
PointAtParameter(ray *Ray, f32 t)
{
    v3 Result = Ray->A + t*Ray->Dir;
    return Result;
}

#define BOUNDING_BOX_FUNCTION(name, ...) b32 name(f32 tMin, f32 tMax, aabb *Result, __VA_ARGS__)
#define HIT_FUNCTION(name, ...) internal b32 name(ray *Ray, f32 tMin, f32 tMax, hit_record *Record, __VA_ARGS__)
#define MATERIAL_FUNCTION(name) internal b32 name(material *Material, ray *Ray, hit_record *Record, v3 *Attenuation, ray *Scattered, random_series *Entropy)

inline f32
HitSphere(ray *Ray, v3 Center, f32 Radius)
{
    v3 OriginCenter = Ray->A - Center;
    v3 Direction = Ray->Dir;

    float A = LengthSq(Direction);
    float B = Inner(OriginCenter, Direction);
    float C = LengthSq(Direction) - Square(Radius);
    float D = B*B - A*C;

    f32 Result = -1;
    if(D >= 0)
    {
        Result = (-B - SquareRoot(D)) / (A);
    }
    
    return Result;
}

inline b32
HitAABB(ray *Ray, aabb *AABB, f32 tMin, f32 tMax)
{
    b32 Result = true;
    
    for(s32 D=0; D<3; ++D)
    {
        f32 InvDir = 1 / Ray->Dir.E[D];

        f32 IntersectMin;
        f32 IntersectMax;
        if(InvDir < 0)
        {
            IntersectMin = (AABB->Max.E[D] - Ray->A.E[D]) * InvDir;
            IntersectMax = (AABB->Min.E[D] - Ray->A.E[D]) * InvDir;
        }
        else
        {
            IntersectMin = (AABB->Min.E[D] - Ray->A.E[D]) * InvDir;
            IntersectMax = (AABB->Max.E[D] - Ray->A.E[D]) * InvDir;
        }

        tMin = Maximum(IntersectMin, tMin);
        tMax = Minimum(IntersectMax, tMax);
        if(tMax <= tMin)
        {
            Result = false;
            break;
        }
    }

    return Result;
}

internal camera
CreateCamera(v3 LookFrom, v3 LookAt, v3 Up, f32 FOV, f32 AspectRatio, f32 Aperture, f32 FocusDistance)
{
    f32 Theta = FOV;
    f32 HalfH = Tan(Theta/2);
    f32 HalfW = AspectRatio * HalfH;

    camera Camera;
    Camera.W = UnitVector(LookFrom - LookAt);
    Camera.U = Cross(UnitVector(Up), Camera.W);
    Camera.V = Cross(Camera.W, Camera.U);
    
    Camera.Origin = LookFrom;
    Camera.LowerLeftCorner = Camera.Origin - HalfW*FocusDistance*Camera.U - HalfH*FocusDistance*Camera.V - FocusDistance*Camera.W;
    Camera.Horizontal = 2*HalfW*FocusDistance*Camera.U;
    Camera.Vertical = 2*HalfH*FocusDistance*Camera.V;

    Camera.LensRadius = Aperture/2;

    return Camera;
}

internal u32 LockedAddAndReturnOriginalValue(u32 volatile *Value, u32 Addend);

internal void CreateWorkThread(void *Parameter);
internal u32 GetCPUCoreCount();
