/* ========================================================================
   $File: $
   $Date: $
   $Revision: $
   $Creator: Lubomir Kurcak $
   $Notice: (C) Copyright 2019 All Rights Reserved. $
   ======================================================================== */

#include <windows.h>

internal u32
LockedAddAndReturnOriginalValue(u32 volatile *Value, u32 Addend)
{
    u32 Result = InterlockedExchangeAdd(Value, Addend);
    return Result;
}

internal DWORD WINAPI
WorkerThread(void *Parameter)
{
    work_queue *Queue = (work_queue *)Parameter;
    while(DrawToImageBufferRegion(Queue));
    return 0;
}

internal void
CreateWorkThread(void *Parameter)
{
    DWORD ThreadID;
    HANDLE ThreadHandle = CreateThread(0, 0, WorkerThread, Parameter, 0, &ThreadID);
    CloseHandle(ThreadHandle);
}

internal u32
GetCPUCoreCount()
{
    SYSTEM_INFO Info;
    GetSystemInfo(&Info);
    u32 Result = Info.dwNumberOfProcessors;;
    return Result;
}

