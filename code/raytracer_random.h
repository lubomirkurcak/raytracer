/* ========================================================================
   $File: $
   $Date: $
   $Revision: $
   $Creator: Lubomir Kurcak $
   $Notice: (C) Copyright 2018 All Rights Reserved. $
   ======================================================================== */

struct random_series
{
    u32 Value;
};

inline u32
GetRandomU32(random_series *Series)
{
    u32 R = Series->Value;
    R = R*7001 + 100003;
    R ^= R << 13;
    R ^= R >> 17;
    R ^= R << 5;
    
    Series->Value = R;

    return R;
}

// NOTE(lubomir): Returns f32 from interval [0, 1)
inline f32
GetRandomF32(random_series *Series)
{
    f32 Result = GetRandomU32(Series) / 4294967296.0f;
    return Result;
}

inline v3
GetRandomV3FromUnitSphere(random_series *Series)
{
    v3 V;

    do
    {
        V.x = Lerp(-1, 1, GetRandomF32(Series));
        V.y = Lerp(-1, 1, GetRandomF32(Series));
        V.z = Lerp(-1, 1, GetRandomF32(Series));
    } while(LengthSq(V) > 1.0f);

    return V;
}

inline v3
GetRandomV3FromUnitDisk(random_series *Series)
{
    v3 V;

    do
    {
        V.x = Lerp(-1, 1, GetRandomF32(Series));
        V.y = Lerp(-1, 1, GetRandomF32(Series));
        V.z = 0;
    } while(LengthSq(V) > 1.0f);

    return V;
}
