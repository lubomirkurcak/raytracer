/* ========================================================================
   $File: $
   $Date: $
   $Revision: $
   $Creator: Lubomir Kurcak $
   $Notice: (C) Copyright 2019 All Rights Reserved. $
   ======================================================================== */

#include <math.h>

#define Pi32 3.14159265358979f

struct v3
{
    union
    {
        struct
        {
            f32 x,y,z;
        };
        struct
        {
            f32 r,g,b;
        };
        f32 E[3];
    };
};

struct v2i
{
    s32 X;
    s32 Y;
};

struct recti
{
    v2i Min;
    v2i Max;
};

inline f32
Square(f32 Value)
{
    f32 Result = Value*Value;
    return Result;
}

inline s32
DivideRoundUp(s32 A, s32 B)
{
    s32 Result = (A + B - 1)/B;
    return Result;
}

inline f32
SafeRatio0(f32 A, f32 B)
{
    f32 Result = 0;
    if(B != 0.0f)
    {
        Result = A/B;
    }

    return Result;
}

inline f32
SafeRatio1(f32 A, f32 B)
{
    f32 Result = 1;
    if(B != 0.0f)
    {
        Result = A/B;
    }

    return Result;
}

inline s32
RoundF32ToS32(f32 Value)
{
    s32 Result = (s32)(Value+0.5f);
    return Result;
}

inline f32
Lerp(f32 A, f32 B, f32 t)
{
    f32 Result = (1-t)*A + t*B;
    return Result;
}

inline v3
operator*(f32 A, v3 B)
{
    v3 Result;
    Result.x = A*B.x;
    Result.y = A*B.y;
    Result.z = A*B.z;

    return Result;
}

inline v3
operator*(v3 B, f32 A)
{
    v3 Result = A*B;
    return Result;
}

inline v3
operator-(v3 A)
{
    v3 Result;
    Result.x = -A.x;
    Result.y = -A.y;
    Result.z = -A.z;

    return Result;
}

inline v3
operator+(v3 A, v3 B)
{
    v3 Result;
    Result.x = A.x + B.x;
    Result.y = A.y + B.y;
    Result.z = A.z + B.z;

    return Result;
}

inline v3
operator-(v3 A, v3 B)
{
    v3 Result;
    Result.x = A.x - B.x;
    Result.y = A.y - B.y;
    Result.z = A.z - B.z;

    return Result;
}

inline v3
V3(f32 x, f32 y, f32 z)
{
    v3 Result = {x,y,z};
    return Result;
}

inline f32
Inner(v3 A, v3 B)
{
    f32 Result = A.x*B.x + A.y*B.y + A.z*B.z;
    return Result;
}

inline v3
Cross(v3 A, v3 B)
{
    v3 Result;
    Result.x = A.y*B.z - A.z*B.y;
    Result.y = A.z*B.x - A.x*B.z;
    Result.z = A.x*B.y - A.y*B.x;

    return Result;
}

inline v3
Lerp(v3 A, v3 B, f32 t)
{
    v3 Result = (1-t)*A + t*B;
    return Result;
}

inline f32
LengthSq(v3 A)
{
    f32 Result = Inner(A, A);
    return Result;
}

inline f32
SquareRoot(f32 Value)
{
    f32 Result = sqrtf(Value);
    return Result;
}

inline f32
Sin(f32 Value)
{
    f32 Result = sinf(Value);
    return Result;
}

inline f32
Cos(f32 Value)
{
    f32 Result = cosf(Value);
    return Result;
}

inline f32
Tan(f32 Value)
{
    f32 Result = tanf(Value);
    return Result;
}

inline f32
Exp(f32 Value)
{
    f32 Result = expf(Value);
    return Result;
}

inline f32
Ln(f32 Value)
{
    f32 Result = logf(Value);
    return Result;
}

inline f32
Pow(f32 Base, f32 Exponent)
{
    f32 Result = Exp(Exponent * Ln(Base));
    return Result;
}

inline s32
GetWidth(recti A)
{
    s32 Result = A.Max.X - A.Min.X;
    return Result;
}

inline s32
GetHeight(recti A)
{
    s32 Result = A.Max.Y - A.Min.Y;
    return Result;
}

inline v2i
GetSize(recti A)
{
    v2i R;
    R.X = GetWidth(A);
    R.Y = GetHeight(A);

    return R;
}

inline v2i
operator+(v2i A, v2i B)
{
    v2i Result;
    Result.X = A.X + B.X;
    Result.Y = A.Y + B.Y;

    return Result;
}

inline v2i
operator-(v2i A, v2i B)
{
    v2i Result;
    Result.X = A.X - B.X;
    Result.Y = A.Y - B.Y;

    return Result;
}

inline v2i
operator*(v2i A, s32 B)
{
    v2i Result;
    Result.X = A.X * B;
    Result.Y = A.Y * B;

    return Result;
}

inline v2i
operator/(v2i A, s32 B)
{
    v2i Result;
    Result.X = A.X / B;
    Result.Y = A.Y / B;

    return Result;
}

inline recti
RectiMinDim(v2i Min, v2i Dim)
{
    recti R;
    R.Min = Min;
    R.Max = Min + Dim;

    return R;
}

inline recti
Intersect(recti A, recti B)
{
    recti R;
    R.Min.X = Maximum(A.Min.X, B.Min.X);
    R.Min.Y = Maximum(A.Min.Y, B.Min.Y);
    R.Max.X = Minimum(A.Max.X, B.Max.X);
    R.Max.Y = Minimum(A.Max.Y, B.Max.Y);

    return R;
}

inline recti
CenteredIn(recti Region, v2i Size)
{
    recti Result;
    v2i Padding = (GetSize(Region) - Size)/2;
    Result.Min = Region.Min + Padding;
    Result.Max = Region.Max - Padding;

    return Result;
}

// -----


inline f32
Length(v3 A)
{
    f32 Result = SquareRoot(LengthSq(A));
    return Result;
}

inline v3
UnitVector(v3 A)
{
    v3 Result = (1/Length(A)) * A;
    return Result;
}

inline v3
ProjectOnPlane(v3 A, v3 N)
{
    v3 Result = A - Inner(A, N)*N;
    return Result;
}

inline v3
Reflect(v3 A, v3 N)
{
    v3 Result = A - 2*Inner(A, N)*N;
    return Result;
}

inline v3
Hadamard(v3 A, v3 B)
{
    v3 Result;
    Result.x = A.x*B.x;
    Result.y = A.y*B.y;
    Result.z = A.z*B.z;

    return Result;
}

inline b32
Refract(v3 A, v3 N, f32 RefractiveIndexRatio, v3 *ResultVector)
{
    b32 Result = false;
    
    v3 UnitA = UnitVector(A);
    f32 Dot = Inner(UnitA, N);
    f32 Discriminant = 1 - Square(RefractiveIndexRatio)*(1 - Square(Dot));

    if(Discriminant > 0)
    {
        *ResultVector = RefractiveIndexRatio*(UnitA - N*Dot) - N*SquareRoot(Discriminant);
        Result = true;
    }
    
    return Result;
}

inline f32
Schlick(f32 Cosine, float RefractiveIndex)
{
    f32 r0 = Square((1-RefractiveIndex)/(1+RefractiveIndex));
    f32 Result = r0 + (1 - r0)*powf(1-Cosine, 5);
    return Result;
}

inline f32
DegreesToRadians(f32 Degrees)
{
    f32 Radians = Degrees * (Pi32/180);
    return Radians;
}

inline f32
RadiansToDegrees(f32 Radians)
{
    f32 Degrees = Radians * (180/Pi32);
    return Degrees;
}

inline f32
GammaToLinear(f32 Value)
{
    f32 Result = Pow(Value, 2.2f);
    return Result;
}

inline f32
LinearToGamma(f32 Value)
{
    f32 Result = Pow(Value, 1/2.2f);
    return Result;
}

internal f32
ExactLinearTosRGB(f32 L)
{
    if(L < 0.0f)
    {
        L = 0.0f;
    }
    
    if(L > 1.0f)
    {
        L = 1.0f;
    }
    
    f32 S = L*12.92f;
    if(L > 0.0031308f)
    {
        S = 1.055f*Pow(L, 1.0f/2.4f) - 0.055f;
    }
    
    return(S);
}

