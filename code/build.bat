@echo off

REM -wd     - Disables specified warning
REM -wd4189 - Unreferenced variable
REM -wd4505 - Unreferenced function
REM -wd4100 - Unreferenced formal parameter
REM -wd4201 - Nameless struct/union
REM -wd4456 - Disable warnings about hiding previous local declaration
REM -wd4127 - Conditional expression is constant
REM -wd4311 - 'type cast': pointer truncation from 'void *' to 'u32'
REM -wd4302 - 'type cast': truncation from 'void *' to 'u32'
REM -wd4010 - single-line comment contains line-continuation character
REM -wd4312 - 'type cast': conversion from 'GLuint' to 'void *' of greater size
REM -MT     - Include C Runtime Library (not dll)
REM -nologo - Disable cl compiler logos
REM -Gm-    - Disables minimal rebuild
REM -GR-    - Disables run-time type information
REM -EHa-   - Disables exception handling
REM -Od     - Disables code optimalizations
REM -Ox     - Enables 'maximum' optimalizations
REM -Oi     - Enables compiler intrinsics
REM -WX     - Consider warnings as errors
REM -W4     - Set warning level to 4 (use -Wall for all warnings)
REM -FC     - Use full paths for errors
REM -Z7     - Create files for debugger
REM -Fm     - Create a map of all functions
set CommonCompilerFlags=-Od -MTd -nologo -fp:fast -Gm- -GR- -EHa- -Oi -WX -W4 -wd4302 -wd4311 -wd4127 -wd4456 -wd4201 -wd4100 -wd4189 -wd4505 -wd4010 -wd4312 -FC -Z7

REM incremental:no  - Disbable incremental build
REM opt:ref         - Compiler tries hard to remove unnecessary code
REM user32.lib      - Windows library not sure for what
REM gdi32.lib       - Same
REM winmm.lib       - Windows Multiledia Library, used for setting Sleep granularity
set CommonLinkerFlags=-incremental:no -opt:ref user32.lib gdi32.lib winmm.lib opengl32.lib

IF NOT EXIST ..\build mkdir ..\build
pushd ..\build

REM 64-bit
cl %CommonCompilerFlags% ..\code\raytracer.cpp /link %CommonLinkerFlags%
popd
