/* ========================================================================
   $File: $
   $Date: $
   $Revision: $
   $Creator: Lubomir Kurcak $
   $Notice: (C) Copyright 2018 All Rights Reserved. $
   ======================================================================== */

#define MAX_SCENE_OBJECTS 128
global_variable hitable *GlobalSceneObjects;
global_variable hitable **GlobalSceneObjectRefs;

internal hitable *
NewSceneObject(scene *Scene, hitable_type Type)
{
    Assert(Scene->ObjectCount < MAX_SCENE_OBJECTS);
    s32 ObjectIndex = Scene->ObjectCount++;
    hitable *Object = Scene->Objects + ObjectIndex;
    Object->Type = Type;
    hitable **Ref = Scene->Refs + ObjectIndex;    
    *Ref = Object;

    return Object;
}

internal void
AddObjectToScene(scene *Scene, hitable Hitable)
{
    hitable *Object = NewSceneObject(Scene, Hit_None);
    *Object = Hitable;
}
